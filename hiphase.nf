process hiphase {

  tag "${dataset}/${pat_name}/${run}""
  label "hiphase_container"
  label "hiphase"

  input:
  tuple val(pat_name), val(run), val(dataset), path(vcf), path(bam)
  tuple path(ref)
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("*phased.vcf"), emit: phased_vcfs

  script:
  """
  AVCF=`echo ${vcf}`
  hiphase \
  ${parstr} \
  --output-vcf \${AVCF%.vcf*}.phased.vcf  \
  --reference ${ref} \
  --vcf ${vcf} \
  --bam ${bam}
  """
}


process hiphase_tumor {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label "hiphase_container"
  label "hiphase"

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcf), path(bam)
  tuple path(ref)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*phased*vcf"), emit: phased_vcfs

  script:
  """
  AVCF=`echo ${vcf}`
  hiphase \
  --output-vcf \${AVCF%.vcf*}.phased.\${CHR}.vcf \
  --reference ${ref} \
  --vcf ${vcf} \
  --bam ${bam} \
  ${parstr}
  """
}
